#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
use Data::Dumper;

my $file = $ARGV[0];
if ($file eq '') {
    # check default cred file
    if (-e '../.Credentials/.credentials') {
        $file = '../.Credentials/.credentials';
    }
    else {
        print "Error!\n";
        exit();
    }
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
my $userid = $config{'DBUSER'};
my $userpass = $config{'DBPASS'};
my $host = $config{'DBHOST'};


##################
## Load in data ##
##################
print "Downloading DGV data...\n";
my $url = "http://dgv.tcag.ca/dgv/docs/GRCh37_hg19_variants_2015-07-23.txt";
my $dgvfile = `mktemp`;
chomp($dgvfile);
system("wget -v -O $dgvfile $url");
my $out = `echo $?`;
chomp($out);
if ($out ne 0) {
    die("\t=> Toronto DGV Data file not found !!\n\n\t=>UPDATE FAILED !!!!\n");
}

=cut
my $dgvfile = "/tmp/dgv.txt";
=cut

open IN, $dgvfile;
my $header = <IN>;

my %studies;
my $invcount =0;
while (my $line = <IN>) {
    chomp($line);
    if ($line eq '') {
        next;
    }
    my @parts = split(/\t/,$line);
    my $type = $parts[5]; 				# variantsubtype: can be loss,insertion,gain+loss,gain,duplication,deletion,sequence alteration,inversion,complex
    my $paper = $parts[6];				# reference
    my $pubid = $parts[7];				# pubmedid
    my $method = $parts[8];				# method
    my $samples = $parts[14];				# cohort description is empty, changed to samplesize
    my $gain = $parts[15] || 0;				# observedgains
    my $loss = $parts[16] || 0;				# observedlosses

    if (! $studies{$pubid}) {
        $studies{$pubid} = { pubid => $pubid, paper => $paper, method => $method, samples => $samples, loss => 0, gain => 0, inv => 0 };
    }
    $studies{$pubid}{loss} += $loss;
    $studies{$pubid}{gain} += $gain;
    if ($type =~ m/inversion/) {
        $studies{$pubid}{inv}++;
        $invcount++;
    }

}
close IN;

system("rm $dgvfile");


#########################
## Connect to database ##
#########################
# get current genome build/database to update
my $dbh = DBI->connect("dbi:mysql:CNVanalysis-hg19:$host",$userid,$userpass);


##############################
## Change DGV Studies Table ##
##############################
## Merge all data from same paper and delete residual ids
my $check = "SELECT * FROM DGV_studies ORDER BY id ASC";
my $sth = $dbh->prepare($check);
$sth->execute();
my $data = $sth->fetchall_arrayref();
$sth->finish();

my %pubids;
my %oldids;
foreach my $pub (@$data) {
    my $dbid = $$pub[0];
    my $pubid = $$pub[1];
    $pubids{$pubid}{counts}++;
    push(@{$pubids{$pubid}{ids}}, $dbid);
}

foreach my $pubid (keys %pubids) {
    my @ids = @{$pubids{$pubid}{ids}};
    my $id  = shift @ids;
    ## multiple of same paper, delete all but lowest id
    if (@ids) {
        my $printids = join(",", @ids);
        my $delete = "DELETE FROM DGV_studies WHERE id in ($printids)";
        $sth = $dbh->prepare($delete);
        $sth->execute();
        $sth->finish();
        foreach my $todelete (@ids) {
            $oldids{$todelete} = $id;
        }

    }
    ## update study info
    if (exists $studies{$pubid}) {
        my $paper = $studies{$pubid}{paper};
        my $gains = $studies{$pubid}{gain} || 0;
        my $losses = $studies{$pubid}{loss} || 0;
        my $inversions = $studies{$pubid}{inv} || 0;
        my $method = $studies{$pubid}{method};
        my $samples = $studies{$pubid}{samples} || 0;
        ## alter remaining study entry
        my $update = "UPDATE DGV_studies SET method = ? , paper = ? , samples = ? , gain = ? , loss = ? , inversion = ? WHERE id = ?";
        $sth = $dbh->prepare($update);
        $sth->execute($method, $paper, $samples, $gains, $losses, $inversions, $id);
        $sth->finish();
    }
    else {
        # unknown (not in DGV, also not in filtered variants, keep as legacy)
    }

}


## Remove deleted dbids from usersettings
$check = "SELECT * FROM usersettings ORDER BY id ASC";
$sth = $dbh->prepare($check);
$sth->execute();
$data = $sth->fetchall_arrayref();
$sth->finish();

foreach my $usersettings (@$data) {
    my %unique;
    my $uid = $$usersettings[0];
    my @usersids = split(",", $$usersettings[2]);
    my @sortedusersids = sort {$a <=> $b} @usersids;
    foreach my $included (@sortedusersids) {
        my $remainingid = $oldids{$included};
        if ($remainingid) {
            $unique{$remainingid} = 1;
        }
        else {
            $unique{$included} = 1;
        }
    }
    my @output = sort {$a <=> $b} keys %unique;
    my $string = join(",", @output);

    my $update = "UPDATE usersettings SET DGVinclude = ? WHERE id = ?";
    $sth = $dbh->prepare($update);
    $sth->execute($string, $uid);
    $sth->finish();
}

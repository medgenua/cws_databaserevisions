ALTER TABLE `CNVanalysis-hg19`.`DGV_studies` DROP COLUMN `gainloss`;
ALTER TABLE `CNVanalysis-hg19`.`DGV_full` DROP COLUMN `gainloss`;
UPDATE `CNVanalysis-hg19`.`Administration` SET `url`='http://dgv.tcag.ca/dgv/docs/GRCh37_hg19_variants_2015-07-23.txt' WHERE `item`='TorontoDGV';


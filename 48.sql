CREATE TABLE IF NOT EXISTS `diagnostic_classes` (`id` int(11) NOT NULL, `sort_order` int(11) NOT NULL COMMENT 'int to determine how to order the options', `name` varchar(255) NOT NULL, `abbreviated_name` varchar(255) NOT NULL, UNIQUE KEY `id` (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `diagnostic_classes` (`id`, `sort_order`, `name`, `abbreviated_name`) VALUES (0, 8, 'NC', 'None'), (1, 1, 'Pathogenic', 'Path'), (2, 3, 'Likely Pathogenic', 'L.Path'), (3, 4, 'Variant of Unknown Significance', 'VUS'), (4, 6, 'Benign', 'Ben'), (5, 7, 'False Positive', 'FP'), (6, 2, 'Pathogenic (Recessive)', 'Path (rec)'), (7, 5, 'Likely Benign', 'L.Ben');
-- merge pathogenic class to class = 1
UPDATE `aberration` SET class = 1 WHERE class = 2
UPDATE `cus_aberration` SET class = 1 WHERE class = 2

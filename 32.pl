#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
use Data::Dumper;

my $file = $ARGV[0];
if ($file eq '') {
    # check default cred file
    if (-e '../.Credentials/.credentials') {
        $file = '../.Credentials/.credentials';
    }
    else {
        print "Error!\n";
        exit();
    }
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
my $userid = $config{'DBUSER'};
my $userpass = $config{'DBPASS'};
my $host = $config{'DBHOST'};

#########################
## Connect to database ##
#########################
# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:CNVanalysis-hg19:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT email FROM users WHERE level = 3");
$gbsth->execute();

my $result = $gbsth->fetchall_arrayref();
$gbsth->finish();

my $admins;
foreach my $line (@$result) {
    my $admin = $$line[0];
    $admins .= "$admin,";
}

$admins = substr($admins,0,-1);
my $mailfile = `mktemp`;

open OUT, ">$mailfile";
print OUT "To: $admins\n";
print OUT "subject: CNV-WebStore source changed\n";
print OUT "from: philip.holmgren\@gmail.com\n\n";
print OUT "Dear,\r\n\r\n";
print OUT "You received this e-mail because you are the administrator of a local CNV-WebStore installation. We have moved our CNV-WebStore code base to BitBucket.\r\n";
print OUT "In order to be get future updates, you need to change some settings in your CNV-WebStore installation files. ";
print OUT "Please follow these steps:\r\n\r\n";
print OUT "1) Login via SSH\r\n\r\n";
print OUT "2) Go to your CNV-WebStore installation folder (default is /CNV-WebStore). This folder will contain several of these subfolders:\r\n";
print OUT "\t- CNVanalysis\r\n";
print OUT "\t- .Credentials (hidden folder)\r\n";
print OUT "\t- Database_Revisions\r\n";
print OUT "\t- Installation_Scripts\r\n";
print OUT "\t- ServerMaintenance\r\n";
print OUT "\t- Web-Interface\r\n\r\n";
print OUT "3) In each of these subfolders, you will find a file 'hgrc' located in the hidden subdirectory '.hg/' (example: CNVanalysis/.hg/hgrc)\r\n";
print OUT "     There should a line specifying the code base URL (for example, for CNVanalysis):\r\n";
print OUT "     default = http://cnv-webstore.ua.ac.be/hg/CNV-WebStore/CNVanalysis\r\n\r\n";
print OUT "4) Change the first part of the URL like this\r\n";
print OUT "     http://cnv-webstore.ua.ac.be/hg/CNV-WebStore/ --> https://bitbucket.org/medgenua/\r\n\r\n";
print OUT "5) Following the example for the CNVanalysis subfolder, you should get this:\r\n";
print OUT "     https://bitbucket.org/medgenua/CNVanalysis\r\n\r\n";
print OUT "6) Repeat this for all of the found subfolders listed in step 2\r\n\r\n";
print OUT "To verify the changes work, go to your CNV-WebStore platform and get the latest updates (for the top menu: Admin --> Platform Updates).\r\n";
print OUT "Apply the changes and you will receive another email to confirm the changes were succesful. ";
print OUT "If you don't receive an e-mail or someting else went wrong, please contact me for further help\r\n\r\n\r\n";
print OUT "Kind regards,\r\nPhilip\r\n\r\n";
close OUT;

## send mail
system("sendmail -t < $mailfile");
system("rm $mailfile");

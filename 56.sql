ALTER TABLE `DGV_gold` ADD `num_total_samples` INT( 11 ) NOT NULL AFTER `num_samples` ;
ALTER TABLE `projsamp` ADD `datatype` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `swgs_gendertest` ;
ALTER TABLE `projsamp` ADD `sampleinfo_file` VARCHAR( 255 ) NOT NULL AFTER `swgs_plots` ;
UPDATE `projsamp` ps JOIN `project` p ON ps.`idproj` = p.`id` SET ps.`datatype` = 'swgs' WHERE p.`datatype`= 'swgs';

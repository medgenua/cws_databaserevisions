UPDATE Administration SET url = 'http://data.omim.org/downloads/AY_R5Ui0TzyyFNh0S-3uWg/morbidmap.txt' WHERE item = 'OmimMorbidMap';
ALTER TABLE genes CHANGE COLUMN symbol symbol VARCHAR(40) NOT NULL;
ALTER TABLE genesum CHANGE COLUMN pubmed pubmed LONGTEXT NULL DEFAULT NULL;

#!/usr/bin/python

import sys
import os
import MySQLdb


def get_count(begin, end, positions):
    count = 0
    for pos in positions:
        if begin <= pos <= end:
            count += 1
    return count


def check_content(content_string, content_structure):
    if content_string == '':
        return False
    # sometimes structure is 1110 with genotypes in content; all faulty strings, so skip
    if content_structure == 3:
        for letter in 'ABNC':
            if letter in content_string:
                return False
    return True


class DBI:
    def __init__(self, database, **login):
        try:
            self.dbc = MySQLdb.connect(db=database, **login)
            self.dbh = self.dbc.cursor()
            self.dbc.autocommit(True)
            self.rows = []
        except:
            sys.exit("Could not connect to database %s with credentials provided: %s" % (database, str(**login)))

    def update_db(self, query):
        self.dbh.execute(query)

    def get_rows(self, query):
        self.dbh.execute(query)
        self.rows = self.dbh.fetchall()
        return self.rows

    def finish(self):
        self.dbc.close()


#####################
## Get Credentials ##
#####################

db_revision_dir = os.getcwd()
basedir = os.path.dirname(db_revision_dir)
default_credentials = os.path.join(basedir, '.Credentials', '.credentials')

try:
    if len(sys.argv) > 1:
        credentialsfile = os.path.abspath(sys.argv[1])
    else:
        credentialsfile = default_credentials

    fh = open(credentialsfile)
    configfile = fh.readlines()
    fh.close()

except IOError:
    sys.exit(
        "Could not open credentials file\nUSAGE: \"perl ApplyUpdates.pl /path/to/credentialsfile\" "
        "or make sure %s is present" % default_credentials)

config = {}
for line in configfile:
    line = line.strip()
    key, value = line.split('=', 1)
    config[key] = value

userid = config['DBUSER']
userpass = config['DBPASS']
host = config['DBHOST']

'''
CONNECT TO DATABASE
'''

print "Connecting to database"

param = {
    'user': userid,
    'passwd': userpass,
    'host': host
}

# Get current genome build/database to update
genomic_build_handle = DBI('GenomicBuilds', **param)
rows = genomic_build_handle.get_rows("SELECT name, StringName FROM CurrentBuild")

if rows:
    ucscbuild, newbuild = genomic_build_handle.rows[0]
    db = "CNVanalysis" + ucscbuild
    genomic_build_handle.finish()
else:
    sys.exit("No genomic build found")

print "\t=> Using current build database (build %s)" % (newbuild,)

##########
## I/ O ##
##########

queries = open('27_queries.txt', 'w')

###################################################################################################
## AIM:                                                                                          ##
##                                                                                               ##
## Correct aberration table                                                                      ##
## Check CNV size: adjust if not correct and update number of SNPs based on datapoints table     ##
###################################################################################################

genome_handle_A = DBI(db, **param)
genome_handle_B = DBI(db, **param)

# Get all adapted regions, omly from diagnostics collection
aid_list = genome_handle_A.get_rows("SELECT DISTINCT(aid) FROM log l LEFT OUTER JOIN aberration a ON a.id = l.aid "
                                    "LEFT OUTER JOIN project p ON a.idproj = p.id WHERE l.entry LIKE 'Region Adapted%' "
                                    "AND p.collection = 'Diagnostics'")

if not aid_list:
    sys.exit("Error ocurred while fetching logs")

for record in aid_list:
    changed = False
    aid_log = record[0]

    # get aberration details
    rows = genome_handle_B.get_rows("SELECT a.id, a.chr, a.start, a.stop, a.size, a.nrsnps, a.idproj, a.sample, "
                                    "p.chiptypeid FROM `aberration` a LEFT OUTER JOIN `project` p ON a.idproj = p.id "
                                    "WHERE a.id = %s" % aid_log)
    if not rows:
        continue

    aid, chromosome, start, stop, size, nrsnps, pid, sid, chipid = genome_handle_B.rows[0]
    try:
        start, stop, nrsnps = int(start), int(stop), int(nrsnps)
    except:
        continue

    # Get latest log entry
    rows = genome_handle_B.get_rows("SELECT entry FROM log WHERE aid = %s and entry LIKE 'Region Adapted%%' "
                                    "ORDER BY id DESC" % aid)
    log_entry = rows[0][0]

    nrsnps_new = nrsnps
    size_new = size

    #  Calculate size
    size_calc = stop - start + 1
    diff = size_calc - size
    if diff != 0:
        changed = True
        size_new = size_calc

    # Check number of probes
    rows = genome_handle_B.get_rows("SELECT content, structure FROM datapoints WHERE id = %s" % aid)

    # Datapoints record might be missing (from lift maybe?)
    if not rows:
        continue
    content, structure = rows[0]
    content = content.strip()
    content = content.strip('_')
    structure = structure.count('1')
    passed = check_content(content, structure)
    if not passed:
        continue

    # parse datapoint string
    try:
        datapoints = [int(x) for x in content.split('_')[0::structure]]
    except:
        continue
    snps_db = get_count(start, stop, datapoints)
    if not snps_db:
        continue

    if snps_db != nrsnps:
        changed = True
        nrsnps_new = snps_db

    # Update Db
    if changed:
        query_update = "UPDATE aberration SET size = %s, nrsnps = %s WHERE id = %s" % (size_new, nrsnps_new, aid)
        genome_handle_B.update_db(query_update)
        queries.write("%s,%s\n" % (aid, query_update))

genome_handle_A.finish()
genome_handle_B.finish()
queries.close()



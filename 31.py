#!/usr/bin/python

import sys
import os
import MySQLdb
from operator import itemgetter


def get_count(begin, end, positions):
    count = 0
    for pos in positions:
        if begin <= pos <= end:
            count += 1
    return count


def check_content(content_string, content_structure):
    if content_string == '':
        return False
    # sometimes structure is 1110 with genotypes in content; all faulty strings, so skip
    if content_structure == 3:
        for letter in 'ABNC':
            if letter in content_string:
                return False
    return True


class DBI:
    def __init__(self, database, **login):
        try:
            self.dbc = MySQLdb.connect(db=database, **login)
            self.dbh = self.dbc.cursor()
            self.dbc.autocommit(True)
            self.rows = []
        except:
            sys.exit("Could not connect to database %s with credentials provided: %s" % (database, str(**login)))

    def update_db(self, query):
        self.dbh.execute(query)

    def get_rows(self, query):
        self.dbh.execute(query)
        self.rows = self.dbh.fetchall()
        return self.rows

    def finish(self):
        self.dbc.close()


#####################
## Get Credentials ##
#####################

db_revision_dir = os.getcwd()
basedir = os.path.dirname(db_revision_dir)
default_credentials = os.path.join(basedir, '.Credentials', '.credentials')

try:
    if len(sys.argv) > 1:
        credentialsfile = os.path.abspath(sys.argv[1])
    else:
        credentialsfile = default_credentials

    fh = open(credentialsfile)
    configfile = fh.readlines()
    fh.close()

except IOError:
    sys.exit(
        "Could not open credentials file\nUSAGE: \"perl ApplyUpdates.pl /path/to/credentialsfile\" "
        "or make sure %s is present" % default_credentials)

config = {}
for line in configfile:
    line = line.strip()
    key, value = line.split('=', 1)
    config[key] = value

userid = config['DBUSER']
userpass = config['DBPASS']
host = config['DBHOST']

'''
CONNECT TO DATABASE
'''

print "Connecting to database"

param = {
    'user': userid,
    'passwd': userpass,
    'host': host
}

# Get current genome build/database to update
genomic_build_handle = DBI('GenomicBuilds', **param)
rows = genomic_build_handle.get_rows("SELECT name, StringName FROM CurrentBuild")

if rows:
    ucscbuild, newbuild = genomic_build_handle.rows[0]
    db = "CNVanalysis" + ucscbuild
    genomic_build_handle.finish()
else:
    sys.exit("No genomic build found")

print "\t=> Using current build database (build %s)" % (newbuild,)

######################################################################################################################
## AIM:                                                                                                             ##
##                                                                                                                  ##
## Correct aberration table: some errors where introduced by:                                                       ##
##  - editcnv: nr genes where not update when region was adapted                                                    ##
##  - datapoints table: merging introduced double data points; combined with the new nr SNP calc made errors        ##
##                                                                                                                  ##
## Corrections:                                                                                                     ##
##  - Check CNV size based on start, stop & update                                                                  ##
##  - Check nr of SNPs based on datepoints (not made unique by Db update and page_class code update                 ##
##  - Check nr of genes and update if necessary                                                                     ##
##                                                                                                                  ##
######################################################################################################################

db_handleA = DBI(db, **param)
db_handleB = DBI(db, **param)

print("Start main query")
# Get all adapted regions, only from diagnostics collection
aberrations = db_handleA.get_rows("SELECT a.id, a.start, a.stop, a.chr, a.nrsnps, a.size, a.nrgenes, d.content, d.structure, p.created "
                                 "FROM aberration a "
                                 "LEFT JOIN project p ON a.idproj = p.id "
                                 "LEFT JOIN datapoints d ON a.id = d.id "
                                 "WHERE p.collection = 'Diagnostics' AND "
                                 "a.LiftedFrom IS NULL AND d.content IS NOT NULL AND "
                                 "d.content != '' AND d.structure IS NOT NULL")

aberrations = sorted(aberrations, key=itemgetter(0))

print("main query finished")
if not aberrations:
    sys.exit("Error occurred while fetching CNVs")

diffs = 0
yeardict = {}
aidlist = []
for cnv in aberrations:
    edit = False
    reason = ''
    # get aberration details
    aid, start, stop, chr, nrsnps, size, nrgenes, content, structure, fulldate = cnv
    try:
        start, stop, nrsnps, size, nrgenes = int(start), int(stop), int(nrsnps), int(size), int(nrgenes)
    except:
        print("Problem making int for CNV %s" % aid)
        quit()

    #  Calculate size
    size_db = stop - start + 1
    if size_db - size:
        edit = True
        reason += "size changed: %i bp - " % (size_db - size)
        size = size_db

    content = content.strip()
    content = content.strip('_')
    structure = structure.count('1')
    if content != '':
        passed = check_content(content, structure)
    else:
        print("Empty datapoints for %s; these shouldn't be selected, check sql query" % aid)
        quit()

    if not passed:
        print("Datapoints error for CNV %s; these should have been gone!" % aid)
        quit()

    # parse datapoint string
    try:
        datapoints = set([int(x) for x in content.split('_')[0::structure]])
    except:
        print("Parsing error for CNV %s" % aid)
        quit()

    # if data points fall out of region, don't update this
    snps_db = get_count(start, stop, datapoints)
    if not snps_db:
        snps_db = nrsnps
        # print("No SNPs for %s?" % aid)

    if snps_db - nrsnps:
        edit = True
        reason += "# SNPs changed: %i -> %i - " % (nrsnps, snps_db)
        nrsnps = snps_db

    # Get nrgenes from table
    genes_db = db_handleB.get_rows("SELECT Count(DISTINCT symbol) as nrgenes FROM genes WHERE chr= '%i' AND "
                        "(((start BETWEEN %i AND %i) OR (end BETWEEN %i AND %i)) OR (start <= %i AND end >= %i))" %
                        (chr, start, stop, start, stop, start, stop))
    genes_db = genes_db[0][0]
    if genes_db - nrgenes:
        edit = True
        reason += "# genes changed: %i would be added - " % (genes_db - nrgenes,)
        nrgenes = genes_db

    # Update Db
    if edit:
        diffs += 1
        date, time = fulldate.split("-")
        date = date.strip()
        day, month, year = date.split("/")
        if year not in yeardict:
            yeardict[year] = 1
        else:
            yeardict[year] += 1

        #print("%s\t%s\t%s" % (aid, reason, year))
        query_update = "UPDATE aberration SET size = %i, nrsnps = %i, nrgenes = %i WHERE id = %s" % (size, nrsnps, nrgenes, aid)
        #print query_update
        db_handleB.update_db(query_update)


print("Number of CNV that change from Db: %s" % diffs)
print("changes by year")
for year, count in yeardict.iteritems():
    print("Year %s: %i" % (year, count))

db_handleA.finish()
db_handleB.finish()



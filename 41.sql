UPDATE  `CNVanalysis-hg19`.`Administration` SET  `url` =  'http://hgdownload.cse.ucsc.edu/goldenPath/hgFixed/database/refLink.txt.gz' WHERE  `Administration`.`item` = "refLink";
UPDATE  `CNVanalysis-hg19`.`Administration` SET  `url` =  'sftpsrv.sanger.ac.uk:decipher-agreements/pub/' WHERE  `Administration`.`item` = "decipher";
UPDATE  `CNVanalysis-hg19`.`Administration` SET  `url` =  'http://dgv.tcag.ca/dgv/docs/GRCh37_hg19_variants_2016-05-15.txt' WHERE  `Administration`.`item` = "TorontoDGV";

from collections import defaultdict
import configparser
from multiprocessing import log_to_stderr
import mysql.connector, mysql.connector.errorcode
from mysql.connector import FieldType
from operator import itemgetter
import os
import re
import sys


debug = False

###########
## MYSQL ##
###########
class MySqlError(Exception):
    pass

class ValueError(Exception):
    pass

# available error codes :
# https://github.com/mysql/mysql-connector-python/blob/master/lib/mysqlx/errorcode.py

# copied from CMG-package, package installation failed due to broken dependencies (most likely due to old Ubuntu server version)
class MySQL:
    def __init__(self, user, password, host, database, allow_local_infile=False):
        try:
            self.cnx = mysql.connector.connect(
                user=user,
                password=password,
                host=host,
                database=database,
                allow_local_infile=allow_local_infile,
            )
        except mysql.connector.Error as err:
            if err.errno == mysql.connector.errorcode.ER_ACCESS_DENIED_ERROR:
                raise MySqlError(
                    "Could not connect to {}@{} as {} : Username/password not valid".format(
                        database, host, user
                    )
                )
            elif err.errno == mysql.connector.errorcode.ER_BAD_DB_ERROR:
                raise MySqlError("Database {}@{} does not exist.".format(database, host))
            else:
                raise MySqlError("Error {} ({}): {}".format(err.errno, err.sqlstate, err.msg))
        self.type = "mysql"
        self.host = host
        self.database = database
        self.cursor = self.cnx.cursor(prepared=False)

    # validate query & params.
    def _checkStatement(self, statement, params):
        # replace ? by %s (stringent match to prevent random replacements)
        if "= ? " in statement:
            print("Replacing ? by %s in the statement. MySQL syntax in python has changed ! ")
            statement = statement.replace("?", "%s")

        try:
            params = self._validatePlaceholders(statement, params)
        except Exception as e:
            raise e
        return statement, params

    # select_db implementation.
    def select_db(self, db):
        # get nonprepared cursor
        try:
            self.cnx.cursor().execute("USE `{}`".format(db))
            self.database = db
            return True
        except mysql.connector.Error as err:
            if err.errno == mysql.connector.errorcode.ER_BAD_DB_ERROR:
                raise MySqlError("Database {}@{} does not exist.".format(db, self.host))
            else:
                raise MySqlError("Error {} ({}): {}".format(err.errno, err.sqlstate, err.msg))

    # shortcut, run this after every statement that alters the database.
    def commit(self):
        self.cnx.commit()

    # utility : does a table exist in current database.
    def tableExists(self, name):
        if not name:
            raise ValueError("No Table name provided.")
        # use buffered cursor to get rowcount.
        csr = self.cnx.cursor(buffered=True)
        csr.execute("SHOW TABLES LIKE %s", [name])
        rc = csr.rowcount
        csr.close()
        if rc:
            return True
        else:
            return False

    # utility : validate provided placeholder values against the query to be launched.
    def _validatePlaceholders(self, statement, params):
        # params can be tuple or dict : if single item => make tuple
        if isinstance(params, str) or isinstance(params, int) or isinstance(params, float):
            print("Param is string: {}".format(params))
            params = (params,)
        # if list : check number of items
        if isinstance(params, list) or isinstance(params, tuple):
            # nr_ele must be equal to nr_%s in statement.
            if len(params) != statement.count("%s"):
                string_params = [str(p) for p in params]
                raise ValueError(
                    "Number of placeholders ({}) does not match provided values ({}): query: {} => placeholders: '{}'".format(
                        len(params), statement.count("%s"), statement, "--".join(string_params)
                    )
                )
            params = tuple(params)
        # if dict : check number and names of items.
        if isinstance(params, dict):
            # nr_keys must be equal to nr_%(k)s in statement
            if len(params) != len(re.findall(r"%\(.*?\)s", statement)):
                raise ValueError(
                    "Number of placeholders does not match provided values: query: {} => placeholders: '{}'".format(
                        statement, "--".join(params.keys())
                    )
                )
            # each element in params must be in statement
            for k in params:
                if not "%({})s".format(k) in statement:
                    raise ValueError(
                        "Placeholder '{}' not found in statement :{}".format(k, statement)
                    )
        # passed
        return params

    # get column names of table
    def getColumns(self, table):
        # table must exist.
        try:
            self.tableExists(table)
        except Exception as e:
            raise ValueError("Table does not exist : {} : {}".format(table, e))
        # get one row.
        try:
            # self.cursor.execute("SELECT * FROM `{}` LIMIT 1".format(table))
            self.cursor.execute("DESCRIBE `{}`".format(table))
            rows = self.cursor.fetchall()
            cols = [row[0] for row in rows]
            dbtypes = [row[1] for row in rows]
            defaults = [row[4] for row in rows]
            self.cursor.reset()
            python_types = list()
            for row in rows:
                if "int" in row[1]:
                    python_types.append(int)
                elif "decimal" in row[1] or "float" in row[1]:
                    python_types.append(float)
                elif "varchar" in row[1] or "text" in row[1]:
                    python_types.append(str)
                elif "timestamp" in row[1]:
                    python_types.append(str)
            return {
                "names": cols,
                "mysql_types": dbtypes,
                "defaults": defaults,
                "python_types": python_types,
            }
        except Exception as e:
            raise MySqlError(e)

    # default select query
    def runQuery(self, statement, params=None, size=None, as_dict=True):
        # validate query & params.
        try:
            statement, params = self._checkStatement(statement, params)
        except Exception as e:
            raise e

        self.fetch_as_dict = as_dict
        try:
            self.cursor.execute(statement, params)
        except mysql.connector.Error as err:
            raise MySqlError("Error {} ({}): {}".format(err.errno, err.sqlstate, err.msg))

        # get column names
        self.columns = [col[0] for col in self.cursor.description]

        # fetch.
        if size:
            self.batch_size = size
            results = self.cursor.fetchmany(size)
            # reset cursor if no items left. otherwise : continue using GetNextBatch
            if len(results) < size:
                self.cursor.reset()
        else:
            results = self.cursor.fetchall()
            self.cursor.reset()

        # build the dictionary (not set as cursor-argument, to make it query-specific)
        if as_dict:
            results = [dict(zip(self.columns, row)) for row in results]

        # return
        return results

    # continue the retrieval of query results using given or stored batch size
    def GetNextBatch(self, size=None):
        if self.cursor.rowcount < 0:
            self.cursor.reset()
            return []
        if size:
            print("Fetching given size of {} rows".format(size))
            results = self.cursor.fetchmany(size)
        elif self.batch_size:
            size = self.batch_size
            print("Fetching {} items".format(self.batch_size))
            results = self.cursor.fetchmany(self.batch_size)
        else:
            raise ValueError("No batch size provided")

        if len(results) < size:
            print("Fetched all results, resetting cursor")
            self.cursor.reset()
        else:
            print("more date is present")
        # build the dictionary
        if self.fetch_as_dict:
            results = [dict(zip(self.columns, row)) for row in results]
        # return
        return results

    # insert into database
    def insertQuery(self, statement, params=None):
        # validate query & params.
        try:
            statement, params = self._checkStatement(statement, params)
        except Exception as e:
            raise e

        # run insert query.
        try:
            self.cursor.execute(statement, params)
            self.commit()
            return self.cursor.lastrowid
        # todo : split on error (duplicate key, missing values, etc)
        except mysql.connector.Error as err:
            if err.errno == mysql.connector.errorcode.ER_WRONG_VALUE_COUNT_ON_ROW:
                raise ValueError(
                    "Error {} : {} : You need to specify column names when importing".format(
                        err.errno, err.msg
                    )
                )
            elif err.errno == mysql.connector.errorcode.ER_DUP_ENTRY:
                raise ValueError("Error {} : {}".format(err.errno, err.msg))
            else:
                raise MySqlError("Error {} ({}): {}".format(err.errno, err.sqlstate, err.msg))

    # queries without return values (update)
    def doQuery(self, statement, params=None):
        # validate query & params.
        try:
            statement, params = self._checkStatement(statement, params)
        except Exception as e:
            raise e

        # run query.
        try:
            self.cursor.execute(statement, params)
            self.commit()
            # reset just to be sure.
            self.cursor.reset()
            return True
        # todo : split errors
        except mysql.connector.Error as err:
            raise MySqlError("Error {} ({}): {}".format(err.errno, err.sqlstate, err.msg))



"""
Get credentials
"""
db_revision_dir = os.getcwd()
basedir = os.path.dirname(db_revision_dir)
default_credentials = os.path.join(basedir, '.Credentials', '.credentials')
if len(sys.argv) > 1:
    credentialsfile = os.path.abspath(sys.argv[1])
else:
    credentialsfile = default_credentials

try:
    # workaround for sectionless config file
    with open(credentialsfile) as f:
        config_string = '[dummy_section]\n' + f.read()
    config = configparser.ConfigParser()
    config.read_string(config_string)
    config = config['dummy_section']
except IOError:
    sys.exit(
        "Could not open credentials file\nUSAGE: \"perl ApplyUpdates.pl /path/to/credentialsfile\" "
        "or make sure %s is present" % default_credentials)

userid = config['DBUSER']
userpass = config['DBPASS']
host = config['DBHOST']

"""
CONNECT TO DATABASE
"""
print("Connecting to database")
# Get current genome build/database to update
db_handler = MySQL(userid, userpass, host, 'GenomicBuilds')
build = db_handler.runQuery("SELECT name, StringName FROM CurrentBuild")

if build:
    ucscbuild = build[0]['name']
    newbuild = build[0]['StringName']
    db = "CNVanalysis" + ucscbuild
    print(f"\t=> Using current build database (build {newbuild})")
else:
    sys.exit("No genomic build found")


"""
Goal of this revision:
Replace all numeric diagnostic classes in the log with the name of the class. This way it becomes more readable to the user 
"""

db_handler.select_db(db)
log_entries = db_handler.runQuery("SELECT id, sid, pid, aid, uid, entry, arguments, time FROM `log` WHERE entry LIKE '%class%'")
dcs = db_handler.runQuery("SELECT id, name FROM diagnostic_classes")
classes = { str(item['id']):item['name'] for item in dcs }
classes['2'] = 'Pathogenic'
classes['0'] = 'Undefined'

entries = dict()
for log in log_entries:
    id = log['id']
    entry_original = entry = log['entry']
    for char in entry_original:
        if char.isdigit():
            entry = entry.replace(char, classes[char])

    # If class is changed to Undefined, sometimes nothing is written e.g.
    # 'Diagnostic Class Changed From 5 to '
    entry = re.sub('From\s{2}to', f"From {classes['0']} to", entry)
    entry = re.sub('to\s*$', f"to {classes['0']}", entry)

    if entry != entry_original:
        entries[log['id']] = {
            'original': entry_original,
            'altered' : entry
        }

print(f"\t=> {len(entries.keys())} log entries will be updated with the name of the class")

if debug:
    uni = dict()
    for key, value in entries.items():
        orig = value['original']
        alt = value['altered']
        uni[orig] = alt
    print(len(uni.keys()))

    for k,v in sorted(uni.items()):
        print(f"'{k}' ==> '{v}'")

with open('backup/log_update_48.txt', 'w') as f:
    for id, value in entries.items():
        params = [value['altered'], id]
        query_update = "UPDATE `log` SET entry=%s WHERE id=%s"
        f.write('\t'.join([str(e) for e in params ]) + '\n')
        if not debug:
            db_handler.doQuery(query_update, params)

print(f"\t=> Finished")

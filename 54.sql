ALTER TABLE `chiptypes` ADD `datatype` VARCHAR( 255 ) NULL DEFAULT NULL ;
UPDATE `CNVanalysis-hg38`.`chiptypes` SET `name` = 'TWIST-EFv2.0', `datatype` = 'swgs' WHERE `chiptypes`.`ID` = 21;
UPDATE `project` SET `chiptype` = 'TWIST-EFv2.0' WHERE `chiptype`= 'sWGS';
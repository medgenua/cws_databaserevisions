CREATE TABLE  `CNVanalysis-hg19`.`Classifier` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`Name` VARCHAR( 255 ) NOT NULL ,`Comment` VARCHAR( 255 ) NOT NULL ,`WholeGenome` TINYINT( 1 ) NOT NULL ,`Gender` VARCHAR( 15 ) NOT NULL ,`CN` VARCHAR( 10 ) NOT NULL ,`public` TINYINT( 1 ) NOT NULL DEFAULT  '0',`uid` INT NOT NULL) ENGINE = MYISAM ;
ALTER TABLE  `Classifier` ADD INDEX (  `public` )
ALTER TABLE  `Classifier` ADD INDEX (  `uid` )
CREATE TABLE  `CNVanalysis-hg19`.`Classifier_x_Userpermissions` (`fid` MEDIUMINT( 9 ) NOT NULL , `uid` MEDIUMINT( 9 ) NOT NULL , `editing` TINYINT( 1 ) NOT NULL DEFAULT  '0', `sharing` TINYINT( 1 ) NOT NULL DEFAULT  '0',PRIMARY KEY (  `fid` ,  `uid` )) ENGINE = MYISAM DEFAULT CHARSET = latin1;

#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;

my $file = $ARGV[0];
if ($file eq '') {
	# check default cred file
	if (-e '../.Credentials/.credentials') {
		$file = '../.Credentials/.credentials';
	}
	else {
		print "Path to credentials file not provided.\n";
		print " USAGE: perl ApplyUpdates.pl /path/to/.dbpass.txt\n";
		exit();
	}
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
$scriptuser = $config{'SCRIPTUSER'};
$scriptpass = $config{'SCRIPTPASS'};

# load conda version for python script
$conda = $config{'CONDA_ENV'};
$cmd = "echo \"$scriptpass\" | sudo -i -S -u $scriptuser conda run -n $conda which python";
$python_binary = `$cmd`;
$python_binary =~ s/\n//g;

# disable output buffer
$|++;

#########################
## Connect to database ##
######################### 
print "Connecting to database\n";

# get current genome build/database to update
my %attr = (PrintError => 0,RaiseError => 0,mysql_auto_reconnect => 1,mysql_local_infile => 1);
my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass,\%attr);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "CNVanalysis".$row[0];
print "\t=> Using current build database (build ".$row[1].")\n";
$mysql = DBI->connect("dbi:mysql:$db:$host",$userid,$userpass,\%attr) or die("Could not connect to database with credentials provided in $file");
#$mysql->{mysql_auto_reconnect} = 1;
#$mysql->{PrintError} = 0;
#$mysql->{RaiseError} = 0;

# get current revision from GenomicBuilds database
my $sth = $mysql->prepare("SELECT Revision FROM `GenomicBuilds`.`DatabaseRevision` ORDER BY Revision DESC LIMIT 1");
my $nrr = $sth->execute();
# if no value in the table, set current rev to 0.
my $currentrev = 0;
if ($nrr > 0) {
	($currentrev) = $sth->fetchrow_array();
}

$rev = $currentrev + 1;
if (! -e "$rev.sql") {
	print "No updates available. Database not changed\n";
	exit();
}
print "Starting update process at Revision : $currentrev\n\n";
# read in .sql names, starting from current revision + 1

my $first = 1;
my $currentsitestatus = '';
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
my $date = ($year + 1900).'-'.($mon + 1).'-'.$mday;

#############################
## REMOVE PREVIOUS BACKUPS ##
#############################
if (-d 'backup') {
	print "\n";
	print " #######################################\n";
	print " # Removing previous database backup ! #\n";
	print " # Hit Ctrl-C to abort !               #\n";	
	print " #######################################\n";
	print "\n";
	sleep 10;
	system("rm -Rf backup");
}	
system("mkdir backup");

## PROCESS
my %dumped = ();
my $lastrev = $rev;
while (-e "$rev.sql") {
	if ($first == 1) {
		# set platform to update
		print "Restricting access to web-interface\n";
		my $stat = $mysql->prepare("SELECT status FROM `GenomicBuilds`.`SiteStatus`");
		$stat->execute();
		($currentsitestatus) = $stat->fetchrow_array();
		$stat->finish();
		$mysql->do("UPDATE `GenomicBuilds`.`SiteStatus` SET status = 'Construction' WHERE 1");
		$first = 0;
	}
	if (-e "$rev.tables") {
		# dump db table for backup
		print "Creating Backup of affected tables before updating (location: backup/)\n";
		open IN, "$rev.tables";
		while (<IN>) {
			chomp($_);
			if ($_ eq '') {
				next;
			}
			my ($dumpdb,$dumptable) = split(/;/,$_);
			if ($dumped{$dumpdb}{$dumptable} == 1) {
				## already dumped, skip
				next;
			}
			print "  => dumping $dumptable from $dumpdb\n";
			$dumped{$dumpdb}{$dumptable} = 1;
			system("mysqldump --user=$userid --password=$userpass --host=$host --quote-names --opt --max_allowed_packet=1G --no-tablespaces $dumpdb $dumptable > backup/$dumpdb--$dumptable.sql");	

		}
		close IN;
	}
	print "Applying updates in $rev.sql\n";
	open IN, "$rev.sql";
	while (<IN>) {
		chomp($_);
		if ($_ eq '') {
			next;
		}
		$mysql->do($_);
		if ($DBI::err) {
			print " #########################\n";
			print " # ERROR : UPDATE FAILED #\n";
			print " #########################\n";
			print "Error while applying update. Database will be reverted to original state (Revision $lastrev).\nError: $DBI::errstr\n";
			# revert !
			print "Restoring database: \n";
			foreach my $loaddb (keys(%dumped)) {
				foreach my $loadtable (keys(%{$dumped{$loaddb}})) {
					system("mysql --user=$userid --password=$userpass --host=$host --max_allowed_packet=1G $loaddb < backup/$loaddb--$loadtable.sql");
				}
			}
			print "Done\n";
			$mysql->do("UPDATE `GenomicBuilds`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
			exit;
		}
		
	}
	close IN;
	## check for Perl script to execute.
	if (-e "$rev.pl") {
		print "Applying update in script $rev.pl\n";
		if (system("perl $rev.pl $file") != 0) {
			print " #########################\n";
			print " # ERROR : UPDATE FAILED #\n";
			print " #########################\n";
			print "Error while applying update in $rev.pl. Database will be reverted to original state (Revision $lastrev).\n";
			# revert !
			print "Restoring database: \n";
			foreach my $loaddb (keys(%dumped)) {
				foreach my $loadtable (keys(%{$dumped{$loaddb}})) {
					system("mysql --user=$userid --password=$userpass --host=$host --max_allowed_packet=1G $loaddb < backup/$loaddb--$loadtable.sql");
				}
			}
			print "Done\n";
			$mysql->do("UPDATE `GenomicBuilds`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
			exit;

		}
	}
	## check for Python script to execute.
	if (-e "$rev.py") {
		print "Applying update in script $rev.py\n";
		if (system("$python_binary -u $rev.py $file") != 0) {
			print " #########################\n";
			print " # ERROR : UPDATE FAILED #\n";
			print " #########################\n";

			print "Error while applying update in $rev.py. Database will be reverted to original state (Revision $lastrev).\n";
			# revert !
			print "Restoring database: \n";
			foreach my $loaddb (keys(%dumped)) {
				foreach my $loadtable (keys(%{$dumped{$loaddb}})) {
					system("mysql --user=$userid --password=$userpass --host=$host --max_allowed_packet=1G $loaddb < backup/$loaddb--$loadtable.sql");
				}
			}
			print "Done\n";
			$mysql->do("UPDATE `GenomicBuilds`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
			exit;

		}
	}
	# store time (auto) and revision in database. In loop so this works for multi commit updates
    $mysql->do("INSERT INTO `GenomicBuilds`.`DatabaseRevision` (Revision) VALUES ('$rev')");
    $lastrev = $rev;
	$rev++;
}
$rev--;
if ($rev == $currentrev) {
	print "No updates available. Database not changed\n";
	exit();
}
else {
	print "Restoring access to Web-Interface\n";
	$mysql->do("UPDATE `GenomicBuilds`.`SiteStatus` SET status = '$currentsitestatus' WHERE 1");
	print "Database Updated successfully to revision : $rev\n";
	print "The original state was stored in backup/\n";
	print "It was left there, in case something did go wrong after all\n";
	
}


ALTER TABLE `GenomicBuilds`.`NewBuild` ADD `size` BIGINT NOT NULL AFTER `Description`;
ALTER TABLE `GenomicBuilds`.`PreviousBuilds` ADD `size` BIGINT NOT NULL AFTER `Description`;
ALTER TABLE `GenomicBuilds`.`CurrentBuild` ADD `size` BIGINT NOT NULL AFTER `Description`;
UPDATE `GenomicBuilds`.`CurrentBuild` SET size = 3137161264 WHERE name = "-hg19"
UPDATE `GenomicBuilds`.`NewBuild` SET size = 3137161264 WHERE name = "-hg19"
UPDATE `GenomicBuilds`.`PreviousBuilds` SET size = 3107677273 WHERE name = "-hg18"


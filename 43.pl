#!/usr/bin/perl


# GOAL (sd+235698): Move all parents from 'Parents' (collection = 5), not in track (sample.intrack) and with LogR Std Dev < 0.1 (projsamp.LRRSD) to new carrier parent projects (collection = 19)
	# make a duplicate of the parent projects and rename project *_Parents to *_CarrierParents and collection
	# move all sample data to new projects
	# enable intrack and update trackfromproject info for these samples
	# make log entry for all changes

# also move samples in existing carrier parent project (HumanCyto12v2.1L_Car_Parents)

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
use Data::Dumper;

my $file = $ARGV[0];
if ($file eq '') {
    # check default cred file
    if (-e '../.Credentials/.credentials') {
        $file = '../.Credentials/.credentials';
    }
    else {
        print "Error!\n";## delete empty project HumanCyto12v2.1L_Car_Parents
$query = $qh->prepare("DELETE FROM project WHERE naam = 'HumanCyto12v2.1L_Car_Parents'");
$query->execute();
$query->finish();

$query = $qh->prepare("DELETE FROM projectpermission WHERE naam = 'HumanCyto12v2.1L_Car_Parents'");
$query->execute();
$query->finish();
        exit();
    }
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
my $userid = $config{'DBUSER'};
my $userpass = $config{'DBPASS'};
my $host = $config{'DBHOST'};

#########################
## Connect to database ##
#########################
# get current genome build/database to update
my $qh = DBI->connect("dbi:mysql:CNVanalysis-hg19:$host",$userid,$userpass);

print "1) Get all parents sid's... ";
# filters: parents from collection = 5, currently not intrack, LogR STDDev < 0.1
my $query = $qh->prepare("SELECT sample.id, project.id, project.naam FROM `sample` JOIN `projsamp` ON projsamp.idsamp = sample.id JOIN `project` ON project.id = projsamp.idproj JOIN collections ON collections.name = project.collection WHERE collections.id = 5 AND sample.intrack = 0 AND projsamp.LRRSD < 0.1");
my $nrresult = $query->execute();
chomp($nrresult);
print "DONE\n";
print "\t$nrresult carrier parents found\n";

my %samples;
my %projects;

# 1 carrier project already exists
$projects{'HumanCyto12v2.1L_Car_Parents'} = 1837;

my $ref  = $query->fetchall_arrayref();
$query->finish();

my $outputfile = '/tmp/carrier_parents.txt';
open my $out, '>', $outputfile;
print $out "SAMPLE\tOLD_PROJECT\tNEW_PROJECT\n";
close $out;

print "2) Change parents' sample data to carrier parent... ";
foreach my $hitref (@$ref) {
	my ($sid, $oldpid, $oldpname) = @{$hitref};
	$samples{$sid}{'oldpid'} = $oldpid;
	$samples{$sid}{'oldpname'} = $oldpname;
	my ($newpid, $newpname);
	$newpname = $oldpname;
	$newcollection = "Carrier_Parents";
	$newpname =~ s/_parents$/_Car_Parents/i;

	# the first sample of a particual project -> duplicate to a carrier parent project
	if (! $projects{$newpname}) {
		## make new project
		$query = $qh->prepare("INSERT INTO project (naam, chiptype, created, collection, userID, chiptypeid, isdefault, asymFilter, minsnp, minmethods, pipeline, plink, finished, protected) SELECT '$newpname', chiptype, created, '$newcollection', userID, chiptypeid, isdefault, asymFilter, minsnp, minmethods, pipeline, plink, finished, protected FROM project WHERE id = $oldpid");
		$query->execute();
		$newpid = $qh->last_insert_id(undef, undef, undef, undef);
		chomp($newpid);
		$query->finish();
		$projects{$newpname} = $newpid;

		## duplicate project permissions
		$query = $qh->prepare("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic, editsample) SELECT '$newpid', userid, editcnv, editclinic, editsample FROM projectpermission WHERE projectid = $oldpid");
		$query->execute();
		$query->finish();

	}

	$newpid = $projects{$newpname};
	$samples{$sid}{'newpid'} = $projects{$newpname};
	$samples{$sid}{'newpname'} = $newpname;

	# open my $out, '>>', $outputfile;
	# print $out "$sid\t$oldpid\t$oldpname\t$newpid\t$newpname\n";
	# close $out;

	print "$sid\t$oldpid\t$oldpname\t$newpid\t$newpname\n";

	## UPDATE sample data tables
	my @queries = (
		"INSERT INTO log (sid, pid, aid, uid, entry, arguments) VALUES ('$sid', '$newpid', '0', '42', 'moved from project \\'$oldpname\\' to \\'$newpname\\' (with a database revision)', '')",
		"UPDATE log SET pid = '$newpid' WHERE pid = '$oldpid' AND sid = '$sid'",
		"UPDATE aberration_LOH SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'", #OK
		"UPDATE aberration SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'", #OK
		"UPDATE BAFSEG SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'", #OK
		"UPDATE deletedcnvs SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'", #OK
		"UPDATE parents_relations SET father_project = '$newpid' WHERE father_project = '$oldpid' AND father = '$sid'", #OK
		"UPDATE parents_relations SET mother_project = '$newpid' WHERE mother_project = '$oldpid' AND mother = '$sid'", #OK
		"UPDATE parents_upd SET fpid = '$newpid' WHERE fid = '$sid' AND fpid = '$oldpid'", #OK
		"UPDATE parents_upd SET mpid = '$newpid' WHERE mid = '$sid' AND mpid = '$oldpid'", #OK
		"UPDATE parents_upd SET spid = '$newpid' WHERE sid = '$sid' AND spid = '$oldpid'", #OK
		"UPDATE plots SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'", #OK
		"UPDATE projsamp SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'", #OK
		"UPDATE sample SET intrack = '1' WHERE id = '$sid'", 	#OK
		"UPDATE sample SET trackfromproject = '$newpid' WHERE id = '$sid'" #OK
	);

	foreach my $query (@queries) {
		$query = $qh->prepare($query);
                $query->execute();
                $query->finish();
	}

}

print "DONE\n";

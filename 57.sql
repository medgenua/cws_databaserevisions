ALTER TABLE `parents_datapoints` ADD `datatype` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `LiftedFrom` ;
UPDATE `db_links` SET `link` = 'https://franklin.genoox.com/clinical-db/variant/sv/chr%c-%s-%e-%d%g' WHERE `db_links`.`Resource` = 'Franklin';

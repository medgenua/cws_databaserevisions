import configparser
import os
from pathlib import Path
import sys

from CMG.DataBase import MySQL

"""
Get credentials
"""
db_revision_dir = os.getcwd()
basedir = os.path.dirname(db_revision_dir)
default_credentials = os.path.join(basedir, '.Credentials', '.credentials')
if len(sys.argv) > 1:
    credentialsfile = os.path.abspath(sys.argv[1])
else:
    credentialsfile = default_credentials

try:
    # workaround for sectionless config file
    with open(credentialsfile) as f:
        config_string = '[dummy_section]\n' + f.read()
    config = configparser.ConfigParser()
    config.read_string(config_string)
    config = config['dummy_section']
except IOError:
    sys.exit(
        "Could not open credentials file\nUSAGE: \"perl ApplyUpdates.pl /path/to/credentialsfile\" "
        "or make sure %s is present" % default_credentials)

dbuser = config['DBUSER']
dbpass = config['DBPASS']
dbhost = config['DBHOST']

"""
CONNECT TO DATABASE
"""

dbh = MySQL(dbuser, dbpass, dbhost, 'GenomicBuilds')
build = dbh.runQuery("SELECT name, StringName FROM CurrentBuild")

if build:
    ucscbuild = build[0]['name']
    newbuild = build[0]['StringName']
    db = "CNVanalysis" + ucscbuild
else:
    sys.exit("No genomic build found")

dbh.select_db(db)


"""
Goal of this revision:
Add the sample_info file path to the database
"""

dbh.select_db(db)
# projsamps_swgs = dbh.runQuery("SELECT * FROM `projsamp` WHERE datatype = 'swgs' AND sampleinfo_file != ''")
projsamps_swgs = dbh.runQuery("SELECT * FROM `projsamp` WHERE datatype = 'swgs'")
for projsamp in projsamps_swgs:
    sid = projsamp['idsamp']
    pid = projsamp['idproj']
    if not projsamp['swgs_plots']:
        continue
    plots = Path(projsamp['swgs_plots'])
    resultsfolder = Path(projsamp['swgs_plots']).parent.parent
    sampleinfofile = os.path.join(resultsfolder, 'sample_info.tsv')
    if os.path.exists(sampleinfofile):
        dbh.doQuery(f"UPDATE `projsamp` SET `sampleinfo_file` = '{sampleinfofile}' WHERE `idsamp` = '{sid}' AND `idproj` = '{pid}' ")
    elif plots:
        print(f"Can't find {sampleinfofile} for {plots}, skipping this sample")

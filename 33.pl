#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;
use Data::Dumper;

my $file = $ARGV[0];
if ($file eq '') {
    # check default cred file
    if (-e '../.Credentials/.credentials') {
        $file = '../.Credentials/.credentials';
    }
    else {
        print "Error!\n";
        exit();
    }
}

# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
my $userid = $config{'DBUSER'};
my $userpass = $config{'DBPASS'};
my $host = $config{'DBHOST'};

#########################
## Connect to database ##
#########################
# get current genome build/database to update
my $dbhgb = DBI->connect("dbi:mysql:CNVanalysis-hg19:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT email FROM users WHERE level = 3");
$gbsth->execute();

my $result = $gbsth->fetchall_arrayref();
$gbsth->finish();

my $admins;
foreach my $line (@$result) {
    my $admin = $$line[0];
    $admins .= "$admin,";
}

$admins = substr($admins,0,-1);
my $mailfile = `mktemp`;

open OUT, ">$mailfile";
print OUT "To: $admins\n";
print OUT "subject: CNV-WebStore source settings changed successful!!\n";
print OUT "from: philip.holmgren\@uantwerpen.be\n\n";
print OUT "Dear,\r\n\r\n";
print OUT "The changes were successful, based on the changes of the Database_Revision subfolder. If you made the same changes to the other subfolders, you should be good.\r\n\r\n";
print OUT "Kind regards,\r\nPhilip\r\n\r\n";
close OUT;

## send mail
system("sendmail -t < $mailfile");
system("rm $mailfile");

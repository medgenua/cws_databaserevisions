#!/usr/bin/python

import sys
import os
import MySQLdb
import re
import time

begin = time.time()

class DBI:
    def __init__(self, database, **login):
        try:
            self.dbc = MySQLdb.connect(db=database, **login)
            self.dbh = self.dbc.cursor()
            self.dbc.autocommit(True)
            self.rows = []
        except:
            sys.exit("Could not connect to database %s with credentials provided: %s" % (database, str(**login)))

    def update_db(self, query):
        self.dbh.execute(query)

    def get_rows(self, query):
        self.dbh.execute(query)
        self.rows = self.dbh.fetchall()
        return self.rows

    def finish(self):
        self.dbc.close()


#####################
## Get Credentials ##
#####################

db_revision_dir = os.getcwd()
basedir = os.path.dirname(db_revision_dir)
default_credentials = os.path.join(basedir, '.Credentials', '.credentials')

try:
    if len(sys.argv) > 1:
        credentialsfile = os.path.abspath(sys.argv[1])
    else:
        credentialsfile = default_credentials

    fh = open(credentialsfile)
    configfile = fh.readlines()
    fh.close()

except IOError:
    sys.exit(
        "Could not open credentials file\nUSAGE: \"perl ApplyUpdates.pl /path/to/credentialsfile\" "
        "or make sure %s is present" % default_credentials)

config = {}
for line in configfile:
    line = line.strip()
    key, value = line.split('=', 1)
    config[key] = value

userid = config['DBUSER']
userpass = config['DBPASS']
host = config['DBHOST']

'''
CONNECT TO DATABASE
'''

print "Connecting to database"

param = {
    'user': userid,
    'passwd': userpass,
    'host': host
}

# Get current genome build/database to update
genomic_build_handle = DBI('GenomicBuilds', **param)
rows = genomic_build_handle.get_rows("SELECT name, StringName FROM CurrentBuild")

if rows:
    ucscbuild, newbuild = genomic_build_handle.rows[0]
    db = "CNVanalysis" + ucscbuild
    genomic_build_handle.finish()
else:
    sys.exit("No genomic build found")

print "\t=> Using current build database (build %s)" % (newbuild,)


###################################################################################################
## AIM:                                                                                          ##
##                                                                                               ##
## Correct datapoints table                                                                      ##
##  - remove double datapoints from content string                                               ##
###################################################################################################

db_handle = DBI(db, **param)

# Get entire diagnostic collection
datapoints = db_handle.get_rows("SELECT d.id, d.content, d.structure FROM datapoints d "
                                "LEFT JOIN aberration a ON a.id = d.id "
                                "LEFT JOIN project p ON a.idproj = p.id "
                                "WHERE p.collection = 'Diagnostics' AND "
                                "d.content IS NOT NULL AND d.content != '' AND "
                                "d.structure IS NOT NULL")

if not datapoints:
    sys.exit("Error ocurred while fetching datapoints")

firstpass = 0
for record in datapoints:
    edit = False
    reason = ''
    aid, content, structure = record
    # Check content syntax
    letternounderscore = re.search('[ABC]\d', content)
    if letternounderscore:
        # AB: genotypes; C: from 'NC' if no logr-only probe
        content = re.sub('([ABC])(\d)', '\g<1>_\g<2>', content)
        edit = True
        reason += 'missing underscore - '
    # no ABCN allowed
    if structure == '1110':
        if any(x in content for x in ['A', 'B', 'C', 'N']):
            structure = '1111'
            edit = True
            reason += 'genotype info but 1110 - '
    # ABNC should be present
    elif structure == '1111':
        if not any(x in content for x in ['A', 'B', 'C', 'N']):
            structure = '1110'
            edit = True
            reason += 'no genotype info but 1111 - '
    if edit:
        firstpass += 1
        query_update = "UPDATE datapoints SET content = '%s', structure = '%s' WHERE id = '%s'" % (content, structure, aid)
        db_handle.update_db(query_update)

print("First check made %i changes" % firstpass)


# split up every string and remove double points
teller = 0
for record in datapoints:
    aid, content, structure = record
    count = structure.count('1')
    content = content.strip()
    content = content.strip("_")
    content = content.split("_")
    positions = content[::count]
    # unique probe positions
    uniques = set(positions)

    # skip if they are all unique
    if len(uniques) == len(positions):
        continue

    #print("%s: deleting %i double datapoints" % (aid, len(positions) - len(uniques)))
    teller += 1

    # get every first encounter of the position in the original list and make new content list

    indexes = [content.index(x) for x in uniques]
    new_content = []
    for i in indexes:
        new_content.extend(content[i:i+count])

    new_string = "_".join(new_content)

    query_update = "UPDATE datapoints SET content = '%s' WHERE id = '%s'" % (new_string, aid)
    db_handle.update_db(query_update)


print("\n%i/%i CNVs altered" % (teller, len(datapoints)))

db_handle.finish()
